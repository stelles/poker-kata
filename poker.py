POWERS = {
    'A' : 14,
    'K' : 13,
    'Q' : 12,
    'J' : 11,
    'T' : 10,
    '9' : 9,
    '8' : 8,
    '7' : 7,
    '6' : 6,
    '5' : 5,
    '4' : 4,
    '3' : 3,
    '2' : 2
}

class Card:
    def __init__(self, num, suite):
        self.num = POWERS[num]
        self.suite =  suite

def getHands(filename):
    hands = [x.split() for x in open(filename,'r').read().splitlines()]
    player1 = []
    player2 = []
    for cards in hands:
        player1.append(sorted([Card(x[0], x[1]) for x in cards[:5]], key=lambda x: x.num))
        player2.append(sorted([Card(x[0], x[1]) for x in cards[5:]], key=lambda x: x.num))
    return player1, player2

def royalFlush(hand):
    suite = hand[0].suite
    for card in hand:
        if not (10 <= card.num <= 14):
            return False
        if card.suite != suite:
            return False
    return True



def straightFlush(hand):
    if straight(hand) and flush(hand):
        return True
    return False


def fourOfKind(hand):
    nums = [card.num for card in hand]
    for num in nums:
        if nums.count(num) == 1:
            kicker = num
    for num in nums:
        if nums.count(num) == 4:
            return num, kicker
    return False

def fullHouse(hand):
    if threeOfKind(hand) and twoOfKind(hand):
        return threeOfKind(hand)[0], twoOfKind(hand)[0]
    return False

def flush(hand):
    suite = hand[0].suite
    for card in hand:
        if card.suite != suite:
            return False
    return True

def straight(hand):
    start = hand[0].num
    for card in hand[1:]:
        if card.num != start + 1:
            if start == 5 and card.num == 14:
                continue
            else:
                return False
        start += 1
    return hand[4].num

def threeOfKind(hand):
    nums = [card.num for card in hand]
    kicker = 0
    for num in nums:
        if nums.count(num) == 1:
            kicker = num
    for num in nums:
        if nums.count(num) == 3:
            return num, kicker
    return False

def twoPair(hand):
    nums = [card.num for card in hand]
    for num in nums:
        if nums.count(num) == 1:
            kicker = num
    for num in nums[:3]:
        if nums[:3].count(num) == 2:
            low = num
    for num in nums[2:]:
        if nums[2:].count(num) == 2:
            high = num
            try:
                return high, low, kicker
            except:
                return False
    return False


def twoOfKind(hand):
    nums = [card.num for card in hand]
    kicker = 0
    for num in nums:
        if nums.count(num) == 1:
            kicker = num
    for num in nums:
        if nums.count(num) == 2:
            return num, kicker
    return False

def highCard(hand):
    return max([card.num for card in hand])

def getPower(hand):
    if royalFlush(hand):
        print "Royal flush"
        return (10, 14)
    if straightFlush(hand):
        return (9, hand[4].num)
    if fourOfKind(hand):
        return (8, fourOfKind(hand)[0], fourOfKind(hand)[1])
    if fullHouse(hand):
        return (7, fullHouse(hand)[0], fullHouse(hand)[1])
    if flush(hand):
        return (6, hand[4].num)
    if straight(hand):
        return (5, hand[4].num)
    if threeOfKind(hand):
        return (4, threeOfKind(hand)[0], threeOfKind(hand)[1])
    if twoPair(hand):
        return (3, twoPair(hand)[0], twoPair(hand)[1], twoPair(hand)[2])
    if twoOfKind(hand):
        return (2, twoOfKind(hand)[0], twoOfKind(hand)[1])
    return (1, hand[4].num, hand[3].num, hand[2].num, hand[1].num, hand[0].num)


def test(p1, p2):
    p1_wins = 0
    for hand in range(len(p1)):

        # print getPower(p1[hand])
        # print getPower(p2[hand])
        p1_hand = getPower(p1[hand])
        p2_hand = getPower(p2[hand])

        for i in range(len(p1_hand)):
            if p1_hand[i] > p2_hand[i]:
                p1_wins += 1
                break
            elif p2_hand[i] > p1_hand[i]:
                break

        # if getPower(p1[hand])[0] > getPower(p2[hand])[0]:
        #     p1_wins += 1
        # elif getPower(p1[hand])[0] == getPower(p2[hand])[0]:
        #     if getPower(p1[hand])[1] > getPower(p2[hand])[1]:
        #         p1_wins +=1
        #     elif getPower(p1[hand])[1] == getPower(p2[hand])[1]:
        #         if getPower(p1[hand])[2] > getPower(p2[hand])[2]:
        #             p1_wins += 1
        #         elif getPower(p1[hand])[2] == getPower(p2[hand])[2]:
        #             if getPower(p1[hand])[3] > getPower(p2[hand])[3]:
        #                 p1_wins += 1

    print "Player 1 won: " + str(p1_wins) + " times."



if __name__ == '__main__':
    player1, player2 = getHands('poker.txt')

    # royal_flush = []
    # for i in ['T', 'J', 'Q', 'K', 'A']:
    #     royal_flush.append(Card(i, 'D'))

    # four = []
    # for i in range(5):
    #     four.append(Card('2', 'D'))

    two_pair = []
    for i in ['2', '2', '3', '3', '4']:
        two_pair.append(Card(i, 'D'))

    print twoPair(two_pair)

    # print fourOfKind(four)
    # print straightFlush(royal_flush)



    test(player1, player2)
